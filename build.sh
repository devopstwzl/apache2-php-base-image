#!/bin/bash
REGISTRY_IMAGE="registry.twizzel.net/docker-builds/apache2-php"
docker build -t $REGISTRY_IMAGE:$1 --build-arg PHP_VERSION=$1 .
#docker push $REGISTRY_IMAGE:$1
