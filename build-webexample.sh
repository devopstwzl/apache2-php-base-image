#!/bin/bash
docker build --no-cache -t registry.twizzel.net/docker-builds/apache2-php-test-webroot:latest --build-arg PHP_VERSION=$1 -f Dockerfile-webexample .
docker push registry.twizzel.net/docker-builds/apache2-php-test-webroot:latest
