#!/bin/bash
#registry.twizzel.net/docker-builds/apache2-php-example:latest
docker stop testdt-php-apache-webexample
docker rm testdt-php-apache-webexample

docker run -it -p 8888:80 -p 8080:443 --name=testdt-php-apache-webexample registry.twizzel.net/docker-builds/apache2-php-example:latest -v /var/twizzel/php81-apache2-test/ssl:/etc/apache2/ssl
