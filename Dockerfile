FROM debian:bullseye-slim
LABEL authors="Tom Sievers <t.sievers@windesheim.nl>"
LABEL description="Debian based image with Apache2 and PHP."

ARG PHP_VERSION=8.2
ENV PHP_VERSION=$PHP_VERSION

# Install packages needed to install PHP
RUN apt-get update
RUN apt-get install -y curl lsb-release ca-certificates apt-transport-https software-properties-common gnupg2
RUN echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/sury-php.list
RUN curl -fsSL  https://packages.sury.org/php/apt.gpg| gpg --dearmor -o /etc/apt/trusted.gpg.d/sury-keyring.gpg
RUN apt-get update

# Setup apache and php
RUN apt-get install -y \
    less \
    cron \
    apache2 \
    php$PHP_VERSION-fpm \
    libapache2-mod-xsendfile \
    php$PHP_VERSION-curl \
    php$PHP_VERSION-gd \
    php$PHP_VERSION-imap \
    php$PHP_VERSION-ldap \
    php$PHP_VERSION-mysql \
    php$PHP_VERSION-readline \
    php$PHP_VERSION-sqlite3 \
    php$PHP_VERSION-xml \
    php$PHP_VERSION-xmlrpc \
    php$PHP_VERSION-soap \
    php$PHP_VERSION-mbstring \
    php$PHP_VERSION-apcu \
    php$PHP_VERSION-imagick \
    php$PHP_VERSION-zip

# Install PHP-JSON in php version below 8.0
RUN case $PHP_VERSION in 8*) echo "PHP-json is included in php-core from 8.0" ;; *) apt-get install -y php$PHP_VERSION-json ;; esac

# Configure the PHP-FPM pool
RUN rm /etc/php/$PHP_VERSION/fpm/pool.d/www.conf
ADD config/php/app-pool.conf /etc/php/$PHP_VERSION/fpm/pool.d/app.conf

# Enabled apache2 modules
RUN a2enmod ssl headers http2 proxy_fcgi setenvif rewrite proxy actions auth_digest authz_groupfile proxy_fcgi proxy_http && a2enconf php$PHP_VERSION-fpm

# Install GOSS
ADD data/goss-linux-amd64 /usr/local/bin/goss
RUN chmod +rx /usr/local/bin/goss

# Clean-up
RUN apt-get -y remove lsb-release ca-certificates apt-transport-https software-properties-common gnupg2 && apt-get autoremove -y && apt-get clean

# Install Apache2 Config
COPY config/apache2/sites-available/*.conf /etc/apache2/sites-available/
RUN a2ensite 000-default && a2ensite 000-default-ssl
COPY config/apache2/apache2.conf /etc/apache2/apache2.conf
COPY config/php/php$PHP_VERSION.ini /etc/php/$PHP_VERSION/apache2/php.ini
RUN mkdir /etc/apache2/ssl

# Goss tests and healthcheck
RUN mkdir /tests
ADD goss-*.yaml /tests
ADD data/healthcheck.sh /
RUN chmod +x /healthcheck.sh
HEALTHCHECK CMD /healthcheck.sh

# Entrypoint script
ADD data/entrypoint.sh /
RUN chmod +x /entrypoint.sh

# Add application (PHP) user
RUN useradd --create-home --home-dir /app --shell /bin/bash app
RUN usermod -a -G app www-data
RUN chmod 750 /app && chown app:app /app
WORKDIR /app/public

#RUN goss --gossfile /test/goss-entrypoint.yaml validate
#RUN goss --gossfile /test/goss-healthcheck.yaml validate

# Entrypoint script (runs as app user)
ENTRYPOINT ["/entrypoint.sh"]
