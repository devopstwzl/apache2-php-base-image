#!/bin/bash

# Create a SSL certificate for internal communication between cluster project and Apache.
SSL_KEY=/etc/apache2/ssl/localhost.key
SSL_CRT=/etc/apache2/ssl/localhost.crt
SSL_CN="/C=NL/ST=Flevoland/L=Almere/O=Windesheim/OU=Docker/CN=localhost/emailAddress=t.sievers@windesheim.nl"

if [ ! -f "$SSL_CRT" ];
then
    echo "Creating new SSL (backend) certificate..."
    echo $SSL_CRT
    echo $SSL_KEY
    echo $SSL_CN
    openssl req -new -newkey rsa:4096 -x509 -sha256 -days 3650 -nodes -out $SSL_CRT -keyout $SSL_KEY -subj "${SSL_CN}"
    # Limit access to SSL key and crt
    chmod 600 $SSL_KEY
    chmod 600 $SSL_CRT
    echo "Done"
fi

# Export the Thorium env variables to PHP.
echo "Exposing Thorium environment variables to PHP..."
env -0 | while IFS='=' read -r -d '' k v;
do
  if [[ $k == THORIUM_* ]];
  then
    printf "env[%s] = %s\n" "$k" "$v" >> /etc/php/$PHP_VERSION/fpm/pool.d/app.conf
  fi
done
chmod 640 /etc/php/$PHP_VERSION/fpm/pool.d/app.conf
echo "Done"

# Test if the container is ready to start running.
if [ "${DISABLE_CHECKS}" != "1" ];
then
  # Run startup/entrypoint check using Goss.
  echo "Running goss (entrypoint) checks..."
  goss -g /tests/goss-entrypoint.yaml validate
  echo "Done"
else
  # Disable (default) health check.
  echo "WARNING: (Goss) entrypoint and health checks are disabled by configuration!"
  echo -e '#!/bin/bash\n\n# Disabled by environment variable\nexit 0' > /dt-healthcheck.sh
fi

# Start the services.
echo "Starting container services..."
service cron start
service cron status
service php$PHP_VERSION-fpm start
service php$PHP_VERSION-fpm status

/usr/sbin/apachectl -D FOREGROUND -k start
