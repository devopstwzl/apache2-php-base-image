#!/bin/bash
docker stop testdt-php-apache
docker rm testdt-php-apache
docker run -it -p 8888:80 -p 8080:443 --name=testdt-php-apache registry.twizzel.net/docker-builds/apache2-php:$1 -v /var/twizzel/php81-apache2-test/ssl:/etc/apache2/ssl
